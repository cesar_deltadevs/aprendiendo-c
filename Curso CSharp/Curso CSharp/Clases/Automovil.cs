﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curso_CSharp.Clases
{
    class Automovil {
        public enum Estados {
            Encendido,
            Apagado
        }
        
        public enum Colores {
            Rojo,
            Plata,
            Azul,
            Negro,
            Blanco
        }

        private Colores color; //Declaracion de una propiedad.
        
        //Esto es el contructor de una propiedad
        public Colores Color {
            get { return color; }
            set { color = value; }
        }

        private string marca;

        public string Marca {
            get { return marca; }
            set { marca = value; }
        }

        private string modelo;

        public string Modelos {
            get { return modelo; }
            set { modelo = value; }
        }

        private Guid numeroDeSerie;

        public Guid NumeroDeSerie {
            get { return numeroDeSerie; }
            //set { numeroDeSerie = value; }
        }

        private int numeroDePasajeros;

        public int NumeroDePasajeros {
            get { return numeroDePasajeros; }
            set {
                if (value < 10) {
                    numeroDePasajeros = value;
                }
                else {
                    numeroDePasajeros = -1;
                }
            }
        }

        public string Tamaño { get; set; }

        public Estados Estado { get; set; }

        public Automovil() {
            numeroDeSerie = Guid.NewGuid();
        }

        public void ProducirAutomovil() {            
            NumeroDePasajeros = 4;
            Color = Automovil.Colores.Plata;
            Marca = "Ford";
            Tamaño = "Compacto";
            imprimirAutomovil();
        }

        public void ProducirAutomovil(Colores color, int numeroDePasajeros) {
            NumeroDePasajeros = numeroDePasajeros;
            Color = color;
            Marca = "Ford";
            Tamaño = "Compacto";
            imprimirAutomovil();
        }
        
        public static void imprimirAutomovil() {
            Console.WriteLine($"Número de serie: {NumeroDeSerie}");
            Console.WriteLine($"Número de pasajeros: {NumeroDePasajeros}");
            Console.WriteLine("Marca: {0}", Marca);
            Console.WriteLine("Color: {0}", color);
            Console.WriteLine($"Tamaño: {Tamaño}");
        }

        public void Encender() {
            Estado = Estados.Encendido;
            imprimirEstado();
        }

        public void Apagar() {
            Estado = Estados.Apagado;
            imprimirEstado();
        }

        protected void imprimirEstado() {
            Console.WriteLine($"Auto: { Estado }");
        }
    }
}
