﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Curso_CSharp.Clases.Automovil;

namespace Curso_CSharp.Clases {
    class FabricaAutomoviles {

        public static List<Automovil> ProducirEnSerie(int numeroAProducir) {
            List<Automovil> autosProducidos = new List<Automovil>();

            for (int i = 0; i < numeroAProducir; i++) {
                Random rdn = new Random(DateTime.Now.Millisecond);
                Task.Delay(100).Wait();

                Colores color = (Colores)rdn.Next(0, 4);
                int numeroDePasajeros = rdn.Next(0, 12);

                var automovilNuevo = Automovil.producirAutomovil(numeroDePasajeros, color);
                autosProducidos.Add(automovilNuevo);
                //automovilNuevo.imprimirAutomovil();
            }

            return autosProducidos;
        }
    }
}
