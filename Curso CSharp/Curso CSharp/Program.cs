﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Curso_CSharp.Clases;

namespace Curso_CSharp
{
    class Program
    {
        static void Main(string[] args) {
            List<Automovil> autosProducidos = FabricaAutomoviles.ProducirEnSerie(5);
            foreach (Automovil autoProducido in autosProducidos) {
                Automovil.impirmirAutomovil();
            }
            Console.Read();

            //bool repetir = true;

            //while (repetir) {
            //    Console.WriteLine("Introduce el número de pasajeros que deseas ");
            //    string numeroDePasajerosTexto = Console.ReadLine();
            //    int numeroDePasajeros = 0;
            //    string repetirTexto;

            //    Automovil MiPrimerAutomovil = new Automovil();
            //    if (int.TryParse(numeroDePasajerosTexto, out numeroDePasajeros)) {
            //        Console.WriteLine("Introduce el color  que deseas ");
            //        string color = Console.ReadLine();
            //        Automovil.Colores colorSeleccionado;
            //        if (Enum.TryParse(color, out colorSeleccionado)) {
            //            switch (colorSeleccionado) {
            //                case Automovil.Colores.Rojo:
            //                case Automovil.Colores.Plata:
            //                case Automovil.Colores.Azul:
            //                    MiPrimerAutomovil.ProducirAutomovil(colorSeleccionado, numeroDePasajeros);
            //                    break;
            //                case Automovil.Colores.Negro:
            //                    Console.WriteLine("No en este color disponible");
            //                    break;
            //                default:
            //                    Console.WriteLine("Color no válido");
            //                    break;
            //            }
            //        }
            //        else {
            //            Console.WriteLine("Error al seleccionar el color");
            //        }
            //        //Colores colorSeleccionado = (Colores)Enum.Parse(typeof(Colores), color);
            //    }
            //    else {
            //        Console.WriteLine("Error en número de pasajeros");
            //    }
            //    Console.WriteLine();

            //    Console.WriteLine("Deseas volver producir otro auto?");
            //    repetirTexto = Console.ReadLine();
            //    if (repetirTexto.ToLower().Equals("no")) {
            //        repetir = false;
            //    }

            //    Console.Clear();
            //}

            ////if(VariableTipoBool) if(miBool)
            ////if(invocacionAMetodo) if(miPrimerAutomovil.ValidarNumero(numero))
            ////if(lineaDeCodigo) if(numeroDePasajeros==4) < > <= >=

            ////Console.ReadKey();
        }
    }
}
